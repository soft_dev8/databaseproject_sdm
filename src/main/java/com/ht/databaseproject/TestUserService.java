/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.databaseproject;

import com.ht.databaseproject.Service.UserService;
import com.ht.databaseproject.model.User;

/**
 *
 * @author ACER
 */
public class TestUserService {
    
    public static void main(String[] args) {      
        UserService userService = new UserService();
        User user = userService.login("user1","password");
        if(user!=null) {
            System.out.println("Welcome user : " + user.getName());
        } else {
            System.out.println("Error");
        }
    } 
    
}
